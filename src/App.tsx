import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { Provider as ReduxProvider } from "react-redux";
import { store } from "./redux/store";
import Consulta from "./module/Consulta";
import { Root } from "@plc/components";

const App = () => {
  return (
    <ReduxProvider store={store}>
      <BrowserRouter basename={"/mesa-operacao"}>
        <Switch>
          <Route path={"/consulta"}>
            <Consulta />
          </Route>
          <Route path={"/configuracao"}>
            <p>Configuração</p>
            <Root name={"Componente compartilhado"} />
          </Route>
        </Switch>
      </BrowserRouter>
    </ReduxProvider>
  );
};

export default App;

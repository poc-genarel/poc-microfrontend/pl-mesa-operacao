import { useAppDispatch, useAppSelector } from "../redux/store";
import { setUser as setUserSlice } from "../redux/slices/userSlice";

export const useUserAuth = () => {
  const dispatch = useAppDispatch();
  const { user } = useAppSelector((state) => state.user);

  const isAuthenticated = () => {
    return !!user;
  };

  const setUser = (user: any) => {
    dispatch(setUserSlice(user));
  };

  return {
    user,
    isAuthenticated,
    setUser,
  };
};

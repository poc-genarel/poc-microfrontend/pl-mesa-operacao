import React, { useEffect } from "react";
import { useAppSelector } from "../../redux/store";
import { useUserAuth } from "../../hook/useUserAuth";

const Consulta = () => {
  const { user } = useAppSelector((state) => state.user);
  const { setUser } = useUserAuth();

  useEffect(() => {
    window.addEventListener("@plc/main/updateUser", (e: any) => {
      setUser(e.detail);
    });
  }, []);

  return (
    <div>
      <p>Consulta {user?.name}</p>
    </div>
  );
};

export default Consulta;
